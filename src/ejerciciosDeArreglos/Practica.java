package ejerciciosDeArreglos;

public class Practica {

	public static void main(String[] args) {
		// 6- Implementar una función static int[] sinrepetidos(int[]a), 
//		que dado un arreglo, retorne un nuevo arreglo sin los elementos 
//		repetidos. Utilizar la función cantRepetidos para saber la cantidad 
//		de elementos del nuevo arreglo

	}

	

	public static int[] sinRepetidos(int[]a) {		
		int[] aux = new int[a.length];
		int contador  = 0;
		
		for (int i = 0; i < a.length; i++) {
			contador = 1;
			for (int j = 0; j < a.length ; j++) {
				if (a[i]==a[j]) {					
					contador += 1;
					if (contador > 1) {
						break;
					}
				}
			}
			if(contador == 1) {
				aux[i] = a[i] ;
			} 			
		}
		
		System.out.print("La cantidad de repetidos es:");
	
		
		return aux;
	}
	
	
}
