package ejerciciosDeArreglos;

public class MetodosPracticaDos {

	public static void main(String[] args) {
		// metodos de la practica dos

	}

	public static int cantRepetidos (int[] arreglos) {
		int repetidos = 0;
		int contador  = 0;
		int[] aux = new int[arreglos.length];
		aux = arreglos.clone();
		for (int i = 0; i < arreglos.length; i++) {
			contador = 0;
			for (int j = 0; j < aux.length ; j++) {
				if (arreglos[i]==aux[j]) {
					//bandera = true;
					aux[i] = -1;
					contador += 1;
				}
			}
			if(contador > 1) {
				repetidos += 1;
			}				
		}
		System.out.print("La cantidad de repetidos es:");
		return repetidos;
	}

	public static int promedio(int[] arreglo) {
		int suma = 0;

		for (int i = 0; i < arreglo.length; i++) {
			suma += arreglo[i];
		}
		System.out.print("El promedio es:");
		return (suma / arreglo.length);
	}

}
