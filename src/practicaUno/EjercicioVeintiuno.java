package practicaUno;

import java.util.Scanner;

public class EjercicioVeintiuno {

	public static void main(String[] args) {
		/*
		 * Escribir un metodo static boolean esSinRepetidos(String s) que 
		 * dada una cadena, devuelve true si no hay letras repetidas en la 
		 * cadena. En caso contrario, devuelve false. No utilizar el metodo
		 * del ejercicio 22.
		 */

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una palabra");
		String cadena = scan.nextLine();
		System.out.println(esSinRepetidos(cadena));
		scan.close();
	}
	
	public static boolean esSinRepetidos (String cadena) {
		boolean bandera = true;
		int i = 0;
		int ocurrencia = 0;
		while(bandera && i < cadena.length()) {
			ocurrencia = EjercicioDiecisiete.cantidadApariciones(cadena,cadena.charAt(i));
			if (ocurrencia > 1) {
				bandera = false;
				
			}
			i++;
		}
		return bandera;
	}

}
