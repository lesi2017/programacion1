package practicaUno;

import java.util.Scanner;

public class EjercicioDieciocho {

	public static void main(String[] args) {
		/*
		 * Escribir un metodo static int cantidadVocales(String s) que dada 
		 * una cadena que contiene solo letras minusculas sin acentuar,
		 *  devuelve la cantidad de vocales en dicha cadena. Nota: se puede  
		 *  utilizar el metodo definida en el ejercicio anterior.
		 */
 
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una palabra");
		String cadena = scan.nextLine();
		scan.close();
		
	
	System.out.println("Esta cadena tiene " + cantidadVocales(cadena)+ " vocales");	

	
	}
	
	public static int cantidadVocales(String cadena){
		String vocales = "aeiou";
		int res = 0;
		for(int i = 0;i < vocales.length(); i++) {
			res += cantidadApariciones(cadena,vocales.charAt(i));
			
		}
		return res;
	}
	
	public static int cantidadApariciones(String cadena, char letra) {
		int cant = 0;
		for (int i = 0; i < cadena.length(); i++) {

			if (cadena.charAt(i) == letra) {
				cant++;
			}
		}
		return cant;
	}
	

}
