package practicaUno;

import java.util.Scanner;


public class EjercicioDiez {
	

	public static void main(String[] args) {
		/*Escribir un metodo static int sumatoria(int n) que devuelve la sumatoria de los n´umeros
		*desde 1 hasta n.
		*/
		System.out.println("Ingrese un número");
		Scanner scan = new Scanner(System.in);
		
		int n = scan.nextInt();
		scan.close();
		System.out.println(sumatoria(n));
		

	}
	
	public static int sumatoria(int n){
		int res = 0;
		for(int i = 1; i<=n ;i++) {
			res += i;
		}
		return res;
	}

}
