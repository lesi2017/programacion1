package practicaUno;

public class EjercicioVeintiocho {

	public static void main(String[] args) {
		/*
		 * Escribir un metodo static boolean estaOrdenado(int[] a) que dado
		 *  un arreglo de enteros,devuelve verdadero si el arreglo esta
         *ordenado crecientemente de menor a mayor, y falso en caso
          *contrario.
		 * */
		
		
		int num[] = {1,3,3,3,4,2};

		System.out.println(estaOrdenado(num));
	}
	
	
	public static boolean estaOrdenado (int[] num) {
	 boolean bandera = true;
	 int i = 0;
	 while (bandera && i < num.length-1) {
		if (num[i] > num[i+1]) {
			bandera = false;
		
		}
		
		i++;
	}
		return bandera;
	}

}
