package practicaUno;

import java.util.Scanner;

public class EjercicioDiecisiete {

	public static void main(String[] args) {
		/*
		 * Escribir un metodo static int cantidadApariciones(String s, char c)que dada
		 * una cadena y un caracter, cuenta la cantidad de veces que aparece c en s
		 */

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una palabra");
		String cadena = scan.nextLine();
		System.out.println("Ingrese una letra");
		char letra = scan.next().charAt(0);
		scan.close();

		System.out.println(cantidadApariciones(cadena, letra));

	}

	public static int cantidadApariciones(String cadena, char letra) {
		int cant = 0;
		for (int i = 0; i < cadena.length(); i++) {

			if (cadena.charAt(i) == letra) {
				cant++;
			}
		}
		return cant;
	}

}
