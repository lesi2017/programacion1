package practicaUno;

public class Shingo {
	public static void main(String[] args) {
		int[] arreglo = {1,2,1,2,5,4,5,5,5,5,5, 55, 4, 0,0};
		
		for (int i = 0; i < sinRepetidos(arreglo).length; i++) {
			System.out.println(sinRepetidos(arreglo)[i]);
		}
		
	}
	
    public static int[] sinRepetidos(int[] arreglo) {
    	//Objeter longitud del arreglo final
    	int len = arreglo.length - cantRepetidos(arreglo);
    	//Crear arreglo final sin repetidos
    	int[] arregloFinal = new int[len];
    	//Crear e inicializar indice del arreglo final
    	int index = 0;
		
    	//Recorrer el arreglo recibido por argumento
    	for(int i = 0; i < arreglo.length; i++) {
    		//Asumir que de entrada hay un numero que 
    		//se guardara en el arreglo final
    		int contador = 1;
    		//Recorro el arreglo final que de entrada esta inicializada en
    		//[0, 0]
    		for(int j = 0; j < arregloFinal.length; j++) {   
    			// Si encuentra un valor del arreglo en el arreglo final, incrementar contador
    			if( arreglo[i] == arregloFinal[j]){    
    				contador++;
    				break;
                }    			
    		}	
    		//Si contador es 1, agregar el valor al arreglo final
    		//Si contador es mayor a 1, no se agrega por estar ya inclido en el arreglo final
    		if (contador == 1) {
    			arregloFinal[index] = arreglo[i];
    			//incremtar indice del arreglo final
    			index++;
			}
    	}
    	
    	return arregloFinal;
    }
    
    public static int cantRepetidos(int[] a) {
        int cont = 0;

        for (int i = 0; i < a.length; i++) {
          for (int j = i; j < a.length; j++) {
            if (a[i] == a[j] && i != j) {
              cont++;
//              System.out.println("cont: " + cont + " i: "+ i +" j: " +j);
              break;
            }

          }
        }

        return cont;
      }

}
