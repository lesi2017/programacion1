package practicaUno;

import java.util.Scanner;



public class EjercicioVeinte {

	public static void main(String[] args) {
		/*
		 * Escribir el metodo static boolean esCapicua(String s) que dada una cadena,
		 * devuelve true si la cadena es igual de atras hacia adelante o de adelante
		 * hacia atr´as. En caso contrario, devuelve false
		 */

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una palabra");
		String cadena = scan.nextLine();
		scan.close();
		
		System.out.println(esCapicuaDos(cadena));
	}

	public static boolean esCapicua(String cadena) {
		String capicua = "";
		for (int i = cadena.length() - 1; i >= 0; i--) {
			capicua += cadena.charAt(i);
		}

		return capicua.equals(cadena);
	}

	// otro metodo mas elegante

	public static boolean esCapicuaDos(String cadena) {
		int i = cadena.length() / 2;
		int j = 1;
		System.out.println(i);
		boolean bandera = true;

		while (i > 0 && bandera) {
			if (cadena.charAt(j - 1) != cadena.charAt(cadena.length() - j)) {
				bandera = false;
			}

			i--;
			j++;
		}

		return bandera;
	}

}
